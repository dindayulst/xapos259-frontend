import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../base/config';
import { OrderDetail } from '../model/orderDetail';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    }),
  }

  public getOrderDetail(): Observable<OrderDetail[]>{
    return this.http.get<OrderDetail[]>(
      `${Config.url}api/order`,
      this.httpOptions
    )
  }

  public addOrder(orderDetail: OrderDetail): Observable<OrderDetail>{
    return this.http.post<OrderDetail>(
      Config.url + 'api/order',
      orderDetail, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  


}
