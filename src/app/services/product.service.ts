import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../base/config';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json',
      'Access-Control-Allow-Origin' : "*",
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }),
  };

  public getProduct(): Observable<Product[]>{
    return this.http.get<Product[]>(
      `${Config.url}api/product`,
      this.httpOptions
    )
  }

  public addProduct(product: Product): Observable<Product>{
    return this.http.post<Product>(Config.url + 'api/product',
    product, {
     ...this.httpOptions,
     responseType: 'text' as 'json', 
    }
    )
  }

  public editProduct(product: Product): Observable<Product>{
    return this.http.put<Product>(
      Config.url + 'api/product',
      product, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public deleteProduct(id: number): Observable<void>{
    return this.http.delete<void>(
      Config.url + `api/product/${id}`,{
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }
}
