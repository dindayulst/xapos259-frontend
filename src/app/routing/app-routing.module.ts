import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../guards/auth.guard";
import { CategoryComponent } from "../pages/category/category.component";
import { HomeComponent } from "../pages/home/home.component";

import { LoginComponent } from "../pages/login/login.component";
import { OrderComponent } from "../pages/order/order.component";
import { ProductComponent } from "../pages/product/product.component";
import { RegisterComponent } from "../pages/register/register.component";
import { VariantComponent } from "../pages/variant/variant.component"; 



const route: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'register', component: RegisterComponent},
    {path: 'category', component: CategoryComponent, canActivate: [AuthGuard]},
    {path: 'variant', component: VariantComponent, canActivate: [AuthGuard]},
    {path: 'product', component: ProductComponent, canActivate: [AuthGuard]},
    {path: 'order', component: OrderComponent, canActivate: [AuthGuard]}

];

@NgModule({
    imports: [
        RouterModule.forRoot(route, {
            initialNavigation: 'enabled'
        }),
    ],
    exports: [RouterModule],
})

export class AppRoutingModule {}