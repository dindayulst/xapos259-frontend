import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './routing/app-routing.module';
import { AuthService } from './services/auth.service';
import { RegisterComponent } from './pages/register/register.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './pages/home/home.component';
import { CategoryComponent } from './pages/category/category.component';
import { VariantComponent } from './pages/variant/variant.component';
import { AuthGuard } from './guards/auth.guard';
import { ProductComponent } from './pages/product/product.component';
import { TopnavComponent } from './mastertheme/topnav/topnav.component';
import { AsidenavComponent } from './mastertheme/asidenav/asidenav.component';
import { FooterComponent } from './mastertheme/footer/footer.component';
import { OrderComponent } from './pages/order/order.component';
 

@NgModule({
  declarations: [AppComponent,LoginComponent, RegisterComponent, NavComponent, HomeComponent, CategoryComponent, VariantComponent, ProductComponent, TopnavComponent, AsidenavComponent, FooterComponent, OrderComponent],
  imports: [BrowserModule,HttpClientModule, FormsModule, AppRoutingModule],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
