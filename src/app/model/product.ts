export interface Product{
    id: number;
    productDescription: string;
    productName: string;
    productPrice: number;
    productStock: number;
    variantId: number;
    productCode: string;
    category: any;
    variant: any;
    

}