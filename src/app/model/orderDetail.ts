export interface OrderDetail{
    id: number;
    headerId: number;
    price: number;
    productId: number;
    quantity: number;

    product: any;
    
}