export interface OrderHeader{
    id: number;
    amount: string;
    reference: string;
}