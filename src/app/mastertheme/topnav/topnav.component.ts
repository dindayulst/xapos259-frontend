import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styles: []
})
export class TopnavComponent implements OnInit {
  token: string = '';

  constructor(private route: Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token')!!;
  }

  public onLogout(){
    localStorage.removeItem('token');
    this.route.navigate(['/login']);
  }

}
