import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/model/category';
import { Variant } from 'src/app/model/variant';
import { VariantService } from 'src/app/services/variant.service';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-variant',
  templateUrl: './variant.component.html',
  styleUrls: ['./variant.component.css']
})
export class VariantComponent implements OnInit {

  public variant: Variant[] = [];
  public editVariant: Variant;
  public deleteVariant: Variant;
  public category: Category[] = [];
  public selectedCategory = 0;

  constructor(private variantService: VariantService, private categoryService: CategoryService) { 
    this.editVariant={} as Variant;
    this.deleteVariant={} as Variant;
  }

  ngOnInit(): void {
    this.getVariant();
    this.getCategory();
  }

  public getCategory() : void {
    this.categoryService.getCategory().subscribe(
      (response: Category[]) => {
        this.category = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getVariant(): void{
    this.variantService.getVariant().subscribe(
      (response: Variant[]) => {
        this.variant = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddVariant(addForm: NgForm): void{
    document.getElementById('');
    this.variantService.addVariant(addForm.value).subscribe(
      (response: Variant) => {
        console.log(response);
        this.getVariant();
        addForm.reset();
      },
      (erorr: HttpErrorResponse) =>{
        alert(erorr.message);
        addForm.reset();
      }
    );
  }

  public onEditVariant(editForm: Variant): void{
    this.variantService.editVariant(editForm).subscribe(
      (response: Variant) => {
        console.log(response);
        this.getVariant();
      },
      (erorr: HttpErrorResponse) =>{
        console.log(erorr.message);
        alert(erorr.message);
      }
    );
  }

  public onDeleteVariant(id: number): void{
    this.variantService.deleteVariant(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getVariant();
      },
      (erorr: HttpErrorResponse) =>{
        console.log(erorr.message);
        alert(erorr.message);
      }
    );
  }

  public onOpenModal(variant: Variant, mode: string): void{
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display ='none';
    button.setAttribute('data-toggle', 'modal');

    if(mode === 'add'){
      console.log('button')
      this.getCategory();
      button.setAttribute('data-target', '#addVariantModal');
    }
    if(mode === 'edit'){
      this.getCategory();
      this.editVariant = variant;
      this.selectedCategory = this.editVariant.categoryId;

      button.setAttribute('data-target', '#editVariantModal');
    }
    if(mode === 'delete'){
      this.deleteVariant= variant;
      button.setAttribute('data-target', '#deleteVariantModal');
    }
    container!.appendChild(button);
    button.click();
  }
  
}