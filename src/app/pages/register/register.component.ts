import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Register } from 'src/app/model/register';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public authRegister: Register = new Register();
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  register(){
    this.authService.register(this.authRegister).subscribe(
      (data: any) => {
        console.log(data);
        this.router.navigate(['login']);
      },
      (err) => {
        console.log(err.error);
      },
      () => console.log('Register Successfully')
    )
  }

}
