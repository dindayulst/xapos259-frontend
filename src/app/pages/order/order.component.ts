import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';
import { OrderDetail } from 'src/app/model/orderDetail';
import { ProductService } from 'src/app/services/product.service';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Product } from 'src/app/model/product';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  public product: Product[] = [];
  public orderDetail: OrderDetail[] = [];
  


  constructor(private orderService: OrderService, private productService: ProductService) { 

  }

  ngOnInit(): void {
    this.getProduct();
  }

  public getProduct(): void{
    this.productService.getProduct().subscribe(
      (response: Product[]) => {
        this.product = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  
  public getOrderDetail(): void {
    this.orderService.getOrderDetail().subscribe(
      (response: OrderDetail[]) => {
        this.orderDetail = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
      )
  }

  public onAddOrder(addForm: NgForm): void{
    document.getElementById('add-orderdetail-form');
    this.orderService.addOrder(addForm.value).subscribe(
      (response: OrderDetail) => {
        console.log(response);
        this.getOrderDetail();
        addForm.reset();
      },
      (error: HttpErrorResponse)=>{
        alert(error.message);
        addForm.reset();
      }
      );
  }

  public onOpenModal(orderDetail: OrderDetail, mode: string): void{
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if(mode === 'add'){
      this.getProduct();
      button.setAttribute('data-target', '#addOrderModal');
    }

    container!.appendChild(button);
    button.click();

  }

}
