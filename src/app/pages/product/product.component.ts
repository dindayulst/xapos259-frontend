import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/model/category';
import { Product } from 'src/app/model/product';
import { Variant } from 'src/app/model/variant';
import { CategoryService } from 'src/app/services/category.service';
import { VariantService } from 'src/app/services/variant.service';
import { ProductService } from 'src/app/services/product.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public category: Category[] = [];
  public variant: Variant[] = [];
  public product: Product[] = [];
  public editProduct: Product;
  public deleteProduct: Product;
  public selectedCategory = 0;
  public selectedVariant = 0;
  


  constructor(
    private categoryService: CategoryService,
    private variantService: VariantService,
    private productService: ProductService 
    ) {
    this.editProduct= {} as Product;
    this.deleteProduct= {} as Product;
   }

  ngOnInit(): void {
    this.getCategory();
    this.getVariant();
    this.getProduct();
    
  }

  public getCategory() : void{
    this.categoryService.getCategory().subscribe(
      (response: Category[]) => {
        this.category = response;
      },
      (error: HttpErrorResponse)=>{
        alert(error.message);
      }
    );
  }

  public getVariant() : void{
    this.variantService.getVariant().subscribe(
      (response: Variant[]) => {
        this.variant = response;
      },
      (error: HttpErrorResponse)=>{
        alert(error.message);
      }
    );
  }

  public getProduct() : void{
    this.productService.getProduct().subscribe(
      (response: Product[]) => {
        this.product = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public variantByCategory(id: any){
    this.variantService.getVariantByCategory(id).subscribe(
      (response: Variant[]) => {
        this.variant = response;
        console.log(this.variant)
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    );
  }

  public onAddProduct (addForm: NgForm): void{
    document.getElementById('');
    this.productService.addProduct(addForm.value).subscribe(
      (response: Product) => {
        console.log(response);
        this.getProduct();
        addForm.reset();
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onEditProduct (editForm: Product): void{
    this.productService.editProduct(editForm).subscribe(
      (response: Product) => {
        console.log(response);
        this.getProduct();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        alert(error.message);
      }
    );
  }

  public onDeleteProduct (id: number): void{
    this.productService.deleteProduct(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getProduct();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        alert(error.message);
      }
    );
  }

  public onOpenModal(product: Product, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if(mode === 'add'){
      this.getCategory();
      this.getVariant();
      button.setAttribute('data-target', '#addProductModal');
    }
    if(mode === 'edit'){
      console.log('edit');
      this.getCategory();
      this.getVariant();
      this.editProduct = product;
      this.selectedCategory = this.editProduct.variant.categoryId;
      this.selectedVariant = this.editProduct.variantId;
      button.setAttribute('data-target', '#editProductModal');
    }
    if(mode === 'delete'){
      console.log('delete');
      this.deleteProduct = product;
      button.setAttribute('data-target', '#deleteProductModal');
    }
    container!.appendChild(button);
    button.click();
  }

}
